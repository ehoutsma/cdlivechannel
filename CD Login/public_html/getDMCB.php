<?php
    include '../include/bootstrap.php';

    $userid = file_get_contents($workFolder.'.deviceId');
    include 'getDRM.php';

    $dmcbUrl = file_get_contents($workFolder.'.dmcburl');
    if ($dmcbUrl != "") {
        $postParameters = $GLOBALS[HTTP_RAW_POST_DATA];
        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $dmcbUrl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.'/cookieZiggo.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.'/cookieZiggo.txt');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, "https://livetv.canaldigitaal.nl/program.aspx");
        curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4);
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: */*',
            'Content-Type: application/octet-stream',
            'Content-Length: ' . strlen($postParameters),
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParameters);

        $output = curl_exec($ch);
        echo($output);
        file_put_contents($workFolder.'test.txt', $output);
        $responseInfo = curl_getinfo($ch);
        $referer = $responseInfo['url'];
        curl_close($ch);
    }