<?php
    include '../include/bootstrap.php';

    $userid = file_get_contents($workFolder.'.deviceId'); //"wdd660bbd-1838-09a0-7f41-d2a1808ade64";
    $id = "";
    if ($_REQUEST['channelurl'] != "") {
            $channelUrl = urldecode($_REQUEST['channelurl']);
            $channels = json_decode(file_get_contents($workFolder.'.channels'));

            foreach ($channels as $channel) {
                    if ($channel->url == $channelUrl) {	
                            $id = $channel->id;
                    }
            }

    }
        
    if ($id != "" && $userid != "") {
        $ch = curl_init(); 
        $startUrl = "https://livetv.canaldigitaal.nl/api.aspx?z=stream&lng=nl&_=1546522053252&u={$userid}&v=1&id={$id}&d=3";
        $postParameters = '{"type":"dash","flags":"4096"}:';
        curl_setopt($ch, CURLOPT_URL, $startUrl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.'/cookieZiggo.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.'/cookieZiggo.txt');

        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
        curl_setopt($ch, CURLOPT_REFERER, "https://livetv.canaldigitaal.nl/program.aspx");

        curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 4); 
        curl_setopt($ch, CURLOPT_TIMEOUT, 4);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            'Accept: application/json',
            'Content-Type: application/json',
            'Content-Length: ' . strlen($postParameters),
            'X-Requested-With: XMLHttpRequest',
        ));
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParameters);

        $response = curl_exec($ch); 
        $json = json_decode($response);
        file_put_contents($workFolder.'.dmcburl', $json->drm->laurl);
        file_put_contents($workFolder.'.inputurl', $json->url);
        curl_close($ch);   
    } else {
        file_put_contents($workFolder.'.dmcburl', "");
        file_put_contents($workFolder.'.inputurl', "");
    }