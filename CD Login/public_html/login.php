<?php
    include '../include/bootstrap.php';
    if ($_SERVER['REQUEST_METHOD'] === 'POST'):
        $UserAgent = "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/70.0.3202.75 Safari/537.36";
        // create curl resource 
        $ch = curl_init(); 
        // set url
	$postParametersArray = ['username'=> $_POST['username'], 'password'=> $_POST['password']]; 
	$postParameters = "";
	foreach ($postParametersArray as $key => $value) {
		if ($postParameters != "") $postParameters .= "&";
		$postParameters .= $key."=".$value;
	}	
	$startUrl = "https://livetv.canaldigitaal.nl/sso.aspx";
        curl_setopt($ch, CURLOPT_URL, $startUrl);
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.'/cookieZiggo.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.'/cookieZiggo.txt');
        //return the transfer as a string 
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1); 
 	curl_setopt($ch, CURLOPT_REFERER, "https://livetv.canaldigitaal.nl");
	curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
        // $output contains the output string 
        $output = curl_exec($ch); 
	$responseInfo = curl_getinfo($ch);
	$referer = $responseInfo['url'];
        curl_close($ch);   
        // close curl resource to free up system resources 


	$ch = curl_init();
        $startUrl = "https://login.canaldigitaal.nl/";
        curl_setopt($ch, CURLOPT_URL, $startUrl);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postParameters);
        curl_setopt($ch, CURLOPT_COOKIEJAR, __DIR__.'/cookieZiggo.txt');
        curl_setopt($ch, CURLOPT_COOKIEFILE, __DIR__.'/cookieZiggo.txt');
        //return the transfer as a string
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_REFERER, $referer);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $UserAgent);
	curl_setopt($ch, CURLINFO_HEADER_OUT, true);
        // $output contains the output string
        $output = curl_exec($ch);
        //curl_close($ch);
	$cookies = curl_getinfo($ch, CURLINFO_COOKIELIST);
	foreach ($cookies as $cookie) {
            if (substr_count($cookie, '%7b%22error%22%3a%22toomany') > 0) {
                $json =	((substr($cookie, strpos($cookie, 'err') + 4)));

                $json = json_decode(urldecode($json));
                $outputDevices = [];
                foreach ($json->devices as $device) {
                        $outputDevices[] = (object)['id'=>$device->id, 'name'=>$device->name];
                }
                file_put_contents($workFolder.'.devices.txt', json_encode($outputDevices));
            }
	}
        
        /* Er mogen niet meer dan 4 devices ingelogd zijn op hetzelfde moment van CD, als bij het inloggen een device ID is meegegeven wordt deze uitlogd. */
        $logoutPart = "";
        if ($_POST['logoutid'] != "")
            $logoutPart = "&ubp=".$_POST['logoutid'];
        
        curl_setopt($ch, CURLOPT_URL, curl_getinfo($ch, CURLINFO_EFFECTIVE_URL).$logoutPart);

	$output = curl_exec($ch);
	
	$cookies = curl_getinfo($ch, CURLINFO_COOKIELIST);
	foreach ($cookies as $cookie) {
            if (substr_count($cookie, 'slcuser_stats') > 0) {
                $json =	((substr($cookie, strpos($cookie, 'slcuser_stats') + 14)));
                $deviceId = $json;
                file_put_contents($workFolder.'.deviceId', $deviceId);

            }
	}
else:
    $devices = json_decode(file_get_contents($workFolder.'.devices.txt'));
?>
<form method=POST>Username: <input type=text name=username><br>Password: <input type=password name=password><br>
<select name=logoutid><option value=""></option><?php
foreach ($devices as $device) {
echo "<option value=\"{$device->id}\">{$device->name}</option>";
}

?></select>
<input type=submit></form>


<?php

endif;

