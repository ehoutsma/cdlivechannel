
/*
 * Copyright (c) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.android.cdlivechannel.player;

import android.content.Context;
import android.net.Uri;

import com.google.android.exoplayer2.offline.FilteringManifestParser;
import com.google.android.exoplayer2.offline.StreamKey;
import com.google.android.exoplayer2.source.MediaSource;
import com.google.android.exoplayer2.source.dash.DashMediaSource;
import com.google.android.exoplayer2.source.dash.manifest.DashManifestParser;
import com.google.android.exoplayer2.upstream.DefaultDataSourceFactory;

import java.util.Collections;
import java.util.List;

public class DashRendererBuilder implements DemoPlayer.RendererBuilder {

    private final Context context;
    private final String userAgent;
    public final Uri uri;

    public DashRendererBuilder(Context context, String userAgent, Uri uri) {
        this.context = context;
        this.userAgent = userAgent;
        this.uri = uri;

    }

    @Override
    public void buildRenderers(DemoPlayer player) {

        MediaSource mMediaSource = new DashMediaSource.Factory(new DefaultDataSourceFactory(context, userAgent))
                .setManifestParser(
                        new FilteringManifestParser<>(new DashManifestParser(), getOfflineStreamKeys(uri)))
                .createMediaSource(uri);


/*
        MediaCodecAudioRenderer audioRenderer = new MediaCodecAudioRenderer(
                context,
                MediaCodecSelector.DEFAULT,
                null,
                true,
                player.getMainHandler(),
                player,
                AudioCapabilities.getCapabilities(context));
*/
        player.onRenderers(null, mMediaSource);
    }

    private List<StreamKey> getOfflineStreamKeys(Uri uri) {
        return Collections.emptyList();
    }

    @Override
    public void cancel() {
        // Do nothing.
    }
}